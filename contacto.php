<!DOCTYPE html>
<html lang="es">
<?php require('require/header.php') ?>
<body>
  <?php require('require/menu2.php'); ?>
  <section id="home">
    <object type="text/html" data="vendor/banner1.html" style="height: 160px; width:100%; overflow:hidden; margin-bottom: 10px;"></object>
  </section>

<div class="container contenido">
  <!-- <h2 class="titulo">Contáctenos</h2> -->
  <div class="row contactos" style="margin: 0;">
      <div class="col l3 m4 s12" style="padding: 0 0;">
        <h4>Contáctenos</h4>
            <form style="padding: 21px 4% 20px 1%;" method="post" id="theForm" class="second" action="mail.php" role="form">
                      <div class="form_row">
                        <div class="input input-field">
                          <i class="material-icons prefix">account_circle</i>
                          <input type="text" id="nombre" class="validate" name="nombre" tabindex="1" required>
                          <label for="nombre">Nombre completo:</label>
                        </div>
                      </div>

                      <div class="form_row">
                        <div class="input input-field">
                          <i class="material-icons prefix">phone</i>
                          <input type="text" id="telefono" class="validate" name="telefono" tabindex="2" required>
                          <label for="telefono">Teléfono:</label>
                        </div>
                      </div>

                      <div class="form_row">
                        <div class="input input-field">
                          <i class="material-icons prefix">settings_cell</i>
                          <input type="text" id="movil" class="validate" name="movil" tabindex="3" required>
                          <label for="movil">Teléfono móvil:</label>
                        </div>
                      </div>

                      <div class="form_row">
                        <div class="input input-field">
                          <i class="material-icons prefix">location_on</i>
                          <input type="text" id="direccion" class="validate" name="direccion" tabindex="4" required>
                          <label for="direccion">Dirección:</label>
                        </div>
                      </div>

                      <div class="form_row">
                        <div class="input input-field">
                          <i class="material-icons prefix">location_city</i>
                          <input type="text" id="ciudad" class="validate" name="ciudad" tabindex="5" required>
                          <label for="ciudad">Ciudad:</label>
                        </div>
                      </div>
                        <div class="form_row">
                          <div class="input input-field">
                            <i class="material-icons prefix">email</i>
                            <label for="email">Su E-mail:</label>
                            <input type="email" id="email" class="validate" name="email" tabindex="6" required>
                          </div>
                        </div>

                        <div class="form_row mensaje">
                          <div class="input input-field">
                            <i class="material-icons prefix">mode_edit</i>
                            <label for="mensaje">Mensaje:</label>
                            <textarea id="mensaje" class="materialize-textarea validate" cols="55" rows="7" name="mensaje" tabindex="7" required></textarea>
                          </div>
                        </div>

                        <div class="form_row botones center-align">
                          <i style="background-color: #0d47a1;" class="submitbtn waves-effect waves-yellow btn z-depth-3 waves-input-wrapper" style=""><input class="waves-button-input" type="submit" tabindex="8" value="Enviar"></i>
                          <!-- <input class="submitbtn waves-effect waves-red" type="submit" tabindex="8" value="Enviar"> </input> -->
                          <!-- <input class="deletebtn waves-effect waves-yellow btn z-depth-3" type="reset" tabindex="9" value="Borrar"> </input> -->
                        </div>
                    <div class="col s12">
                      <div id="statusMessage"></div>
                    </div>
            </form>
        </div>
        <div class="col l9 m8  s12" style="padding: 0 0;">
          <h4>Ubíquenos</h4>
              <iframe style="width: 100%;" src="https://www.google.com/maps/d/u/0/embed?mid=1DrICIS_VvL1ipLZSnRz4nZtdNC0" width="640" height="480"></iframe>
        </div><!-- fin de colf2 -->
    </div><!-- fin de row -->
  </div>
 <?php require('require/footer.php'); ?>
  </body>
</html>
