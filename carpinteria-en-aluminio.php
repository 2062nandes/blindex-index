<!DOCTYPE html>
<html lang="es">
<?php require('require/header.php') ?>
<body>
  <?php require('require/menu2.php'); ?>
  <section id="home">
    <object type="text/html" data="vendor/banner1.html" style="height: 160px; width:100%; overflow:hidden; margin-bottom: 10px;"></object>
  </section>

<div class="container contenido">
  <!-- <h2 class="titulo">Contáctenos</h2> -->
  <div class="row contactos" style="margin: 0;">
      <div class="col l12 m12 s12" style="padding: 0 0;">
        <h2>Carpintería en Aluminio</h2>
        <article id="carpinteria-en-aluminio">
          <!-- <h3 class="col s12 m12 l8 derecha">Vidrios Templados</h3> -->
          <div class="col s12 m12 l4 imagen">
              <img class="materialboxed col s12" data-caption="Carpintería en Aluminio" width="250" src="images\cristal-templado\Cristal-templado-5.jpg">
          </div>
            <p>Dedicados al diseño de cualquier tipo de trabajo en aluminio, además a la fabricación y montaje de todo tipo de ventanas, puertas, fachadas flotante, muro-cortinas, pasamanos, mamparas, boxes para duchas y mucho más, debido a su calidad y confort, convirtiéndonos en especialistas de carpintería de aluminio.</p>
            <p>Los productos de aluminio carece de mantenimiento, ya que no atrae la suciedad gracias a sus acabados lacados. Las averías son mínimas, gracias al proceso de anodizado garantiza la correcta adherencia de la pintura e impide la corrosión parcial o total. Así, es más duradera, no se deteriora ni se deforma son una garantía contra la humedad, el sol y cualquier incidencia metrológico soporta la radiación solar, no son inflamables en caso de incendio no desprende sustancias nocivas para la salud. El aluminio es ecológico respeta el medio ambiente. Para su formación precisa de un bajo coste energético, no es tóxico y es RECICLABLE.</p>
        </article>
      </div>
    </div><!-- fin de row -->
  </div>
 <?php require('require/footer.php'); ?>
  </body>
</html>
