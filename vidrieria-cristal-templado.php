<!DOCTYPE html>
<html lang="es">
<?php require('require/header.php') ?>
<body>
  <?php require('require/menu2.php'); ?>
  <section id="home">
    <object type="text/html" data="vendor/banner1.html" style="height: 160px; width:100%; overflow:hidden; margin-bottom: 10px;"></object>
  </section>

<div class="container contenido">
  <!-- <h2 class="titulo">Contáctenos</h2> -->
  <div class="row contactos" style="margin: 0;">
      <div class="col l12 m12 s12" style="padding: 0 0;">
        <h2>Vidriería Cristal Templado</h2>
        <article id="vidrios-templados">
          <h3 class="col s12 m12 l8 derecha">Vidrios Templados</h3>
          <div class="col s12 m12 l4 imagen">
              <img class="materialboxed col s12" data-caption="Vidrios Templados" width="250" src="images\cristal-templado\Cristal-templado-5.jpg">
          </div>
            <p>El cristal templado es un tipo de vidrio de seguridad, procesado por tratamientos térmicos o químicos, para aumentar su resistencia en comparación con el vidrio normal. Esto se logra poniendo las superficies exteriores en compresión y las superficies internas en tensión. Tales tensiones hacen que el vidrio, cuando se rompe, se desmenuce en trozos pequeños granulares en lugar de astillar en grandes fragmentos dentados.</p>
            <p>Los trozos granulares tienen menos probabilidades de causar lesiones.</p>
        </article>
        <article id="vidrios-laminados">
          <h3 class="col s12 m12 l8">Vidrios laminados</h3>
          <div class="col s12 m12 l4 imagen derecha">
              <img class="materialboxed col s12" data-caption="Vidrios laminados" width="250" src="images\cristal-templado\Cristal-templado-2.jpg">
          </div>
          <p>Se produce mediante la unión de dos o más laminas de vidrio con una o más láminas de elementos plásticos de alta resistencia como refuerzo, lo que permite que al romperse la pieza los trozos de vidrio queden adheridos a ella.</p>
          <p>Una de las características más relevantes de este tipo de vidrio es su alta resistencia al impacto y la penetración, motivo por el cual se lo utiliza para protección de personas y bienes. En caso de rotura, la lámina plástica retiene por adherencia los fragmentos de vidrio, reduciendo así los riesgos de daños en caso de accidente. Ofrece también buenas cualidades ópticas, mejora la atenuación acústica y protege contra la radiación ultravioleta.</p>
        </article>
        <article id="vidrios-doble-acristalamiento">
          <h3 class="col s12 m12 l8 derecha">Vidrios doble acristalamiento</h3>
          <div class="col s12 m12 l4 imagen">
              <img class="materialboxed col s12" data-caption="Vidrios doble acristalamiento" width="250" src="images\cristal-templado\Cristal-templado-1.jpg">
          </div>
          <p>Se fabrica con doble y triple acristalamiento.</p>
          <p>Puede fabricarse con mayor número de cámaras, según el grado de aislamiento y el destino.</p>
          <p>El sistema de doble acristalamiento es una solución eficaz porque reduce el flujo de energía lumínica, térmica y sonora al atravesar el acristalamiento, así disminuye los coeficientes de trasmisión energética y de ruidos.</p>
        </article>
      </div>
    </div><!-- fin de row -->
  </div>
 <?php require('require/footer.php'); ?>
  </body>
</html>
