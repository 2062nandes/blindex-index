

<footer class="page-footer blue darken-3" >
  <div class="container">
    <div class="row">
      <div class="col s12 m12 l7 wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeIn; padding: 2px;">
        <div class="fwrap">
          <h5 class="white-text center-align barra">NUESTROS SERVICIOS</h5>
          <ul id="datos" class="servicios" style="text-align: justify;">
            <div class="center-align" style="padding: 0;">
              <a href="vidrieria-cristal-templado.php">· Vidriería cristal templado</a>
              <a href="carpinteria-en-aluminio.php">· Carpintería en aluminio</a>
              <a href="#!.php">· Ventanas y puertas</a>
              <a href="#!.php">· Fachadas flotantes</a>
              <a href="vidrio-estructural.php">· Vidrio estructural</a>
              <a href="#!.php">· Boxes de ducha</a>
              <a href="#!.php">· Mamparas y mostradores</a>
              <a href="#!.php">· Cubiertas de policarbonato</a>
              <a href="#!.php">· Alucobond</a>
              <a href="#!.php">· Arte diseño esmerilado</a>
            </div>
          </ul>
        </div>
      </div>
      <div class="col l5 m12 s12 wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeIn; padding: 2px;">
        <h5 class="white-text center-align barra">DIRECCIÓN — BOLIVIA</h5>
        <div class="fwrap">
            <div id="datos">
              <p class="grey-text text-lighten-4 center-align"><span style="color:rgba(255, 108, 108, 0.83);">COCHABAMBA:</span> Av. Blanco Galindo # 1634, Km. 2 entre Sarmiento y San Lorenzo (acera norte)</p>
              <p class="grey-text text-lighten-4 center-align"><a href="https://www.google.com/maps/d/u/0/embed?mid=1DrICIS_VvL1ipLZSnRz4nZtdNC0" target="_blank"><i style="font-size: 1.1em" class="fa fa-map-marker"> </i> Ver mapa</a></p>
              <p class="grey-text text-lighten-4 center-align"><i style="font-size: 1.2em" class="fa fa-phone"></i> Telf.: 4280701 <span class="opt"> &nbsp;&nbsp;</span> <i style="font-size: 1.2em" class="fa fa-mobile"></i> Cel.: 72738679</p>
              <p class="white-text center-align"><i style="font-size: .9em" class="fa fa-envelope"></i> blindexvt@gmail.com <span class="opt"> &nbsp;&nbsp;</span></p>
            </div>
        </div>
      </div>
<?php /*
      <div class="col l3 m12 s12 center-align redes-sociales wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeIn; padding: 2px;">
          <h1 class="white-text barra" style="font-size:25px;margin-top: 12px;">BLINDEX BOLIVIA</h1>
          <div class="fwrap">
              <ul id="datos">
              <h2 class="white-text" style="font-size:19px; margin:0;"></h2>
              <p class="white-text">Visitenos en: </p>
              <ul class="social_icons">
                    <li><a href="#"><i class="waves-effect waves-light fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="waves-effect waves-light fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class=" waves-effect waves-light fa fa-google-plus"></i></a></li>
              </ul>
            </ul>
          </div>
      </div>
*/ ?>
    </div>
  </div>
  <div class="barra">
    <div class="container">
      <div class="row" style="margin-bottom: 0px;">
          <div class="col s12 l5 white-text center-align" style="font-size: 15px;margin-top: 1.5%;">
              Todos los Derechos Reservados &REG; <strong style="font-weight:700;">Blindex Bolivia.</strong> &COPY; <?=date("Y");?>.
          </div>
            <div class="col s12 l3 center-align">
              <ul class="social_icons">
                    <li><a href="#"><i class="waves-effect waves-light fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="waves-effect waves-light fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class=" waves-effect waves-light fa fa-google-plus"></i></a></li>
              </ul>
            </div>
          <div class="col s12 l4 white-text center-align" style="color: rgba(255, 255, 255, 0.22);">
              <p class="ah"><a href="//ahpublic.com" target="_blank" id="ahpublicidad">Diseño y Programación Ah! Publicidad</a></p>
          </div>
      </div>
    </div>
  </div>
</footer>
<!-- Modal Trigger -->
<!-- <a class="waves-effect waves-light btn" href="#modal1">Modal</a>
<div id="modal1" class="modal bottom-sheet">
  <div class="modal-content">
    <h4>Modal Header</h4>
    <p>A bunch of text</p>
  </div>
  <div class="modal-footer">
    <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Agree</a>
  </div>
</div> -->
<!--Import jQuery before materialize.js-->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="vendor/materialize/materialize.js"></script>
<script src="js/jquery.prettyPhoto.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript">
  // $(window).scroll(function() {
  //   if($(window).scrollTop() + $(window).height() == $(document).height()) {
  //     //  alert("bottom!");
  //       $('#modal1').modal('open');
  //   }
  // }); SCROLL BOOTOM
  $("#mostrar").hover(function(){
       $('.button-collapse').sideNav('show');
  });
</script>
