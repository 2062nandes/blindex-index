<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
  <link rel="icon" href="/favicon.png" type="image/png">
  <meta name="designer" content="Fernando Javier Averanga Aruquipa"/>
  <meta name="description" content="Carpinteria en aluminio cristal templado | Blindex Bolivia" />
  <meta name="author" content="David Baldiviezo M." />
  <meta name="keywords" content="Blindex Bolivia, Vidriería cristal templado, Carpintería en aluminio, Ventanas y puertas, Fachadas flotantes, Vidrio estructural, Boxes de ducha, Mamparas y mostradores, Cubiertas de policarbonato, Alucobond, Arte diseño esmerilado" />
  <title>Blindex | Carpinteria en aluminio cristal templado</title>
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link type="text/css" rel="stylesheet" href="vendor/materialize/materialize.css"  media="screen,projection"/>
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/prettyPhoto.css" rel="stylesheet">
  <link href="css/animate.css" rel="stylesheet">
  <link href="style.css" rel="stylesheet">
</head>
