<header>
  <nav class="nav-extended" role="navigation" style="height: 150px;    background: linear-gradient(#0054d6,#00153a);">
    <div class="nav-wrapper container row">
      <div class="col s5 m5 l5">
          <!-- BLINDEX LOGO -->
            <!-- <object class="logo-canvas" type="text/html" data="vendor/blindex-logo/BLINDEX.html"></object> -->
        <a id="logo-container"  href="/" class="brand-logo wow fadeIn" data-wow-duration="2.5s" data-wow-delay="0.1s">
              <img class="logo-base" src="images\logo-animado\logo-base.png"  style="width: 401px;margin-top: -12px;margin-left: -50px; position: absolute;" alt="Blindex Bolivia">
              <img class="logo-resplandor-blindex" src="images\logo-animado\logo-resplandor-blindex.png"  style="width: 401px;margin-top: -12px;margin-left: -50px; position: absolute;" alt="Blindex Bolivia">
              <img class="logo-resplandor-icono" src="images\logo-animado\logo-resplandor-icono.png"  style="width: 401px;margin-top: -12px;margin-left: -50px; position: absolute;" alt="Blindex Bolivia">
              <img class="logo-glass" src="images\logo-animado\glass.png" alt="Blindex glass Bolivia">
              <div class="slogan">
                <h3 >Carpintería en aluminio<br>vidrio templado y estructural</h3>
              </div>
        </a>
      </div>
      <ul class="icons-inicio right">
          <li><a class="waves-effect waves-light wow rotateInDownLeft" data-wow-duration="1s" data-wow-delay="2.500s" href="/">Inicio</a></li>
          <li><a class="waves-effect waves-light wow rotateInDownLeft" data-wow-duration="1s" data-wow-delay="2.750s" href="/">Nosotros</a></li>
          <li><a class="waves-effect waves-light wow rotateInDownLeft" data-wow-duration="1s" data-wow-delay="3s" href="contacto.php">Contacto</a></li>
      </ul>
      <ul class="col s9 offset-s3 right hide-on-med-and-down lista-menu">
        <!-- <li><a class="botones waves-effect waves-light btn" style="color: #ffffff;text-align: center;  font-weight: bold;  text-shadow: /* Simulating Text Stroke */ -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000;  border: 1px solid rgba(0, 0, 0, 0.5);  border-bottom: 3px solid rgba(0,0,0,0.5);  -webkit-border-radius: 3px;  -moz-border-radius: 3px;  border-radius: 3px;  background: rgba(181, 181, 181, 0.25);  -o-box-shadow: 0 2px 8px rgba(0,0,0,0.5), inset 0 1px rgba(255,255,255,0.3), inset 0 10px rgba(255,255,255,0.2), inset 0 10px 20px rgba(255,255,255,0.25), inset 0 -15px 30px rgba(0,0,0,0.3);  -webkit-box-shadow: 0 2px 8px rgba(0,0,0,0.5), inset 0 1px rgba(255,255,255,0.3), inset 0 10px rgba(255,255,255,0.2), inset 0 10px 20px rgba(255,255,255,0.25), inset 0 -15px 30px rgba(0,0,0,0.3);  -moz-box-shadow: 0 2px 8px rgba(0,0,0,0.5), inset 0 1px rgba(255,255,255,0.3), inset 0 10px rgba(255,255,255,0.2), inset 0 10px 20px rgba(255,255,255,0.25), inset 0 -15px 30px rgba(0,0,0,0.3);  box-shadow: 0 2px 8px rgba(0,0,0,0.5), /* Exterior Shadow */ inset 0 1px rgba(255,255,255,0.3), /* Top light Line */ inset 0 10px rgba(255,255,255,0.2), /* Top Light Shadow */ inset 0 10px 20px rgba(255,255,255,0.25), /* Sides Light Shadow */ inset 0 -15px 30px rgba(0,0,0,0.3);  display: inline-block; text-decoration: none;"href="/">Inicio</a></li> -->
        <li style="padding: 0 0 0 5px;" class="col s3"><a class="botones waves-effect waves-red btn wow fadeInDown" data-wow-duration="1s" data-wow-delay="0s" href="vidrieria-cristal-templado.php">Vidriería <br>cristal templado</a></li>
        <li style="padding: 0 0 0 5px;" class="col s3"><a class="botones waves-effect waves-red btn wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.250s" href="carpinteria-en-aluminio.php">Carpintería <br>en aluminio</a></li>
        <li style="padding: 0 0 0 5px;" class="col s3"><a class="botones waves-effect waves-red btn wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s" href="/">Ventanas <br>y puertas</a></li>
        <li style="padding: 0 0 0 5px;" class="col s3"><a class="botones waves-effect waves-red btn wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.750s" href="/">Fachadas <br>flotantes</a></li>
        <li style="padding: 0 0 0 5px;" class="col s3 offset-s3"><a class="botones waves-effect waves-red btn wow fadeInDown" data-wow-duration="1s" data-wow-delay="1s" href="vidrio-estructural.php">Vidrio <br>estructural</a></li>
        <li style="padding: 0 0 0 5px;" class="col s3"><a class="botones waves-effect waves-red btn wow fadeInDown" data-wow-duration="1s" data-wow-delay="1.25s" href="/">Boxes<br>de ducha</a></li>
        <li style="padding: 0 0 0 5px;" class="col s3"><a class="botones waves-effect waves-red btn wow fadeInDown" data-wow-duration="1s" data-wow-delay="1.5s" href="/">Mamparas y <br>mostradores</a></li>
        <li style="padding: 0 0 0 5px;" class="col s3 offset-s6"><a class="botones waves-effect waves-red btn wow fadeInDown" data-wow-duration="1s" data-wow-delay="1.75s" href="/">Cubiertas de <br>policarbonato</a></li>
        <li style="padding: 0 0 0 5px;" class="col s3"><a class="botones waves-effect waves-red btn wow fadeInDown" data-wow-duration="1s" data-wow-delay="2s" href="/">Alucobond <br><span style="text-transform:none; font-size:11px;">(Aluminio compuesto)</span></a></li>
        <li style="padding: 0 0 0 5px;" class="col s3 offset-s9"><a class="botones waves-effect waves-red btn wow fadeInDown" data-wow-duration="1s" data-wow-delay="2.25s" href="/">Arte diseño <br>esmerilado</a></li>
      </ul>
      <ul id="nav-mobile" class="side-nav" style="background: rgba(255, 255, 255, 0.79);overflow-x: hidden;overflow-y: scroll;">
        <img src="images\logo.png"  style="width: 310px;margin-top: -12px; margin-left:-10px;margin-right:-10px; margin-bottom:-30px;" alt="">
        <h1 style=" margin-top:0;padding-top:0; color: rgb(0, 29, 179); text-align: center; font-size: 21px;font-weight: 500;">CARPINTERIA EN ALUMINIO CRISTAL TEMPLADO</h1>
        <li class="divider"></li>
        <li class="col s12"><a class="col s4 waves-effect waves-red btn menu" href="/">Inicio</a><a class="col s4 waves-effect waves-red btn menu" href="curriculum.php">Nosotros</a><a class="col s4 waves-effect waves-red btn menu" href="contacto.php">Contacto</a></li>
        <li class="divider"></li>
        <li><a class="waves-effect waves-red btn" href="vidrieria-cristal-templado.php">Vidriería cristal templado</a></li>
        <li><a class="waves-effect waves-red btn" href="carpinteria-en-aluminio.php">Carpintería en aluminio</a></li>
        <li><a class="waves-effect waves-red btn" href="#!.php">Ventanas y puertas</a></li>
        <li><a class="waves-effect waves-red btn" href="#!.php">Fachadas flotantes</a></li>
        <li><a class="waves-effect waves-red btn" href="vidrio-estructural.php">Vidrio estructural</a></li>
        <li><a class="waves-effect waves-red btn" href="#!.php">Boxes de ducha</a></li>
        <li><a class="waves-effect waves-red btn" href="#!.php">Mamparas y mostradores</a></li>
        <li><a class="waves-effect waves-red btn" href="#!.php">Cubiertas de policarbonato</a></li>
        <li><a class="waves-effect waves-red btn" href="#!.php">Alucobond</a></li>
        <li><a class="waves-effect waves-red btn" href="#!.php">Arte diseño esmerilado</a></li>
        </a></li>
      </ul>
      <a href="#" data-activates="nav-mobile" class="button-collapse waves-effect waves-red btn z-depth-3 waves-input-wrapper right"><i class="material-icons">menu</i></a>
    </div>
  </nav>
</header>
