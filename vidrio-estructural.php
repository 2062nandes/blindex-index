<!DOCTYPE html>
<html lang="es">
<?php require('require/header.php') ?>
<body>
  <?php require('require/menu2.php'); ?>
  <section id="home">
    <object type="text/html" data="vendor/banner1.html" style="height: 160px; width:100%; overflow:hidden; margin-bottom: 10px;"></object>
  </section>

<div class="container contenido">
  <!-- <h2 class="titulo">Contáctenos</h2> -->
  <div class="row contactos" style="margin: 0;">
      <div class="col l12 m12 s12" style="padding: 0 0;">
        <h2>Vidrio Estructural</h2>
        <article id="vidrio-estructural">
          <!-- <h3 class="col s12 m12 l8 derecha">Vidrios Templados</h3> -->
          <div class="col s12 m12 l4 imagen derecha">
              <img class="materialboxed col s12" data-caption="Vidrio Estructural" width="250" src="images\cristal-templado\Cristal-templado-5.jpg">
          </div>
            <p>El vidriado estructural técnica de sujeción del acristalamiento mediante el empleo de siliconas especiales: en los muros-cortina tradicionales, es la cara exterior del marco la que recibe los esfuerzos de succión inducidos por el viento e impide que el acristalamiento caiga al vacío mientras que en el sistema estructural, es la junta de silicona la que garantiza esta función. Hoy en día, las posibilidades estéticas del sistema estructural suscitan gran interés entre los arquitectos y proyectistas en virtud de la elegancia, luminosidad y espectacularidad estética de la que dota este sistema de acristalamiento a las edificaciones.</p>
            <p>Es el recurso arquitectónico ideal para llevar la luz allí donde se necesite, es ecológico, porque ayuda a reducir el uso de la electricidad, y existen incluso la posibilidad de que sea estructural y no solo para la fachada también para caminar por un piso acristalado. este material está diseñado para resistir tanto cargas de uso residencial como comercial, con grosores del panel y del marco que varían según las exigencias estéticas y estructurales. Generalmente se emplea un vidrio con la superficie tratada para que sea antideslizante, al tiempo que reduce su transparencia, y puede adquirirse en diferentes tonos de color.</p>
        </article>
        <!-- <article id="vidrios-laminados">
          <h3 class="col s12 m12 l8">Vidrios laminados</h3>
          <div class="col s12 m12 l4 imagen derecha">
              <img class="materialboxed col s12" data-caption="Vidrios laminados" width="250" src="images\cristal-templado\Cristal-templado-2.jpg">
          </div>
          <p>Se produce mediante la unión de dos o más laminas de vidrio con una o más láminas de elementos plásticos de alta resistencia como refuerzo, lo que permite que al romperse la pieza los trozos de vidrio queden adheridos a ella.</p>
          <p>Una de las características más relevantes de este tipo de vidrio es su alta resistencia al impacto y la penetración, motivo por el cual se lo utiliza para protección de personas y bienes. En caso de rotura, la lámina plástica retiene por adherencia los fragmentos de vidrio, reduciendo así los riesgos de daños en caso de accidente. Ofrece también buenas cualidades ópticas, mejora la atenuación acústica y protege contra la radiación ultravioleta.
          </p>
        </article>
        <article id="vidrios-doble-acristalamiento">
          <h3 class="col s12 m12 l8 derecha">Vidrios doble acristalamiento</h3>
          <div class="col s12 m12 l4 imagen">
              <img class="materialboxed col s12" data-caption="Vidrios doble acristalamiento" width="250" src="images\cristal-templado\Cristal-templado-1.jpg">
          </div>
          <p>Se fabrica con doble y triple acristalamiento.</p>
          <p>Puede fabricarse con mayor número de cámaras, según el grado de aislamiento y el destino.</p>
          <p>El sistema de doble acristalamiento es una solución eficaz porque reduce el flujo de energía lumínica, térmica y sonora al atravesar el acristalamiento, así disminuye los coeficientes de trasmisión energética y de ruidos.</p>
        </article> -->
      </div>
    </div><!-- fin de row -->
  </div>
 <?php require('require/footer.php'); ?>
  </body>
</html>
